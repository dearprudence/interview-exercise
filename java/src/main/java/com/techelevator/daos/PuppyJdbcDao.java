package com.techelevator.daos;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import com.techelevator.exceptions.PuppyNotFoundException;
import com.techelevator.models.Puppy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * PuppyJdbcDao
 */
@Component
public class PuppyJdbcDao implements PuppyDao {

    private JdbcTemplate template;

    public PuppyJdbcDao(DataSource datasource) {
        template = new JdbcTemplate(datasource);
    }

    @Override
    public List<Puppy> getPuppies() {
        //TODO: Implement this DAO method
        return null;
    }

    @Override
    public Puppy getPuppy(int id) {
        //TODO: Implement this DAO method
        return null;
    }

    @Override
    public void savePuppy(Puppy puppyToSave) {
        //TODO: Implement this DAO method
        return;
    }
}